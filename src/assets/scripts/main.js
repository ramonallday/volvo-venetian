(function(window) {

  function Venetian3d(el, options) {

    /* helper functions */
    function extend(config, newConfig) {
      if (!newConfig) { return config; }

      for (var option in config) {
        if (typeof newConfig[option] !== 'undefined') {
          config[option] =  newConfig[option];
        }
      }

      return config;

    }

    function shuffle(array) {
      var currentIndex = array.length,
          temporaryValue, randomIndex;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }

    /* From Modernizr */
    function whichTransitionEvent() {
      var t;
      var el = document.createElement('fakeelement');
      var transitions = {
              'transition':'transitionend',
              'OTransition':'oTransitionEnd',
              'MozTransition':'transitionend',
              'WebkitTransition':'webkitTransitionEnd'
            };

      for (t in transitions) {
        if (el.style[t] !== undefined) {
          return transitions[t];
        }
      }
    }

    //classie
    function classReg(className) {
      return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
    }

    function hasClass(elem, c) {
        return classReg(c).test(elem.className);
      }
   
    function addClass(elem, c) {
        if (!hasClass(elem, c)) {
          elem.className = elem.className + ' ' + c;
        }
      }
    function removeClass(elem, c) {
        elem.className = elem.className.replace(classReg(c), ' ');
      }

    function toggleClass(elem, c) {
      var fn = hasClass(elem, c) ? removeClass : addClass;
      fn(elem, c);
    }

    function createElement(className) {
      var el = document.createElement('div');
      el.className = className || '';

      return el;
    }

    function getWindowWidth(maxWidth) {
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        return w < maxWidth ? w : maxWidth;
    }

    /* logic */
    var self = this;
    var targetElement = el;
    var aspectRatio = 250 / 970;
    //limit to 970px because images will look like ****
    var maxWidth = 970;
    var targetElementWidth = getWindowWidth(maxWidth);

    var targetElementHeight = Math.floor(targetElementWidth * aspectRatio);
    var targetStructure = (function() {

      var slides = [],
          backgrounds = [],
          imgs = el.getElementsByTagName('img');

      for (var i = 0; i < imgs.length; i++) {
        backgrounds.push(imgs[i].src);
        slides.push(imgs[i].parentNode);
      }

      return {
        backgrounds: backgrounds,
        slides: slides
      };
    })();

    var defaultOptions = {
      x: 5,
      y: 3,
      random: false  
    };
    
    var o = extend(defaultOptions, options);

    var cardsCount = o.x * o.y;
    var cardsArrayCount = cardsCount - 1
    var rangeArray = Array.apply(null, {
      length: cardsCount
    }).map(Number.call, Number);

    var randomizedArray = (o.random ? shuffle(rangeArray) : rangeArray);
    var transitionEvent = whichTransitionEvent();
    var supportsTransition = !!transitionEvent;
    var nextSlide = supportsTransition ? 2 : 1;
    var lastSideIndex = cardsArrayCount;

    self.isAnimating = false;

    function createWall(className, bgUrl) {
      var el = createElement(className);

      if (bgUrl) {
        el.style.backgroundImage = 'url(' + bgUrl + ')';
      }

      return el;
    }

    function createStructure() {

      //empty slides array
      targetStructure.slides.length = 0;
      targetElement.innerHTML = '';

      for (var i = 0; i < cardsCount; i++) {
        
        var block = createElement('flip-container transition-' + randomizedArray[i]),
            cube = createElement('flipper'),
            slide = {};

        slide.parent = targetElement.appendChild(block);
        slide.wall = block.appendChild(cube);

        //add background-image to front and back
        slide.front = {};
        slide.front.el = cube.appendChild(createElement('front'));
        slide.front.bg = slide.front.el.appendChild(createWall('bg', targetStructure.backgrounds[0]));

        slide.back = {};
        slide.back.el = cube.appendChild(createElement('back'));
        slide.back.bg = slide.back.el.appendChild(createWall('bg', targetStructure.backgrounds[1]));

        targetStructure.slides.push(slide);

        // if random order get element index which will be animated last
        // console.log(randomizedArray[i], i);

        

        if (o.random && cardsArrayCount === randomizedArray[i]) {
          lastSideIndex = i;
        } 

      }

      return targetStructure.slides;

    }

    function positionElement(el, w, h, t, l) {
      el.style.width = w + 'px';
      el.style.height = h + 'px';
      el.style.top = t + 'px';
      el.style.left = l + 'px';
    }

    function positionElements(c) {
      if (!c.length) { return; }

      var cubeWidth = Math.ceil(targetElementWidth / o.x),
          cubeHeight = Math.ceil(targetElementHeight / o.y),
          offsetTop = 0,
          offsetLeft = 0;

      for (var i = 0; i < c.length; i++) {

        //new line every o.x
        if (i && i % o.x === 0) {
          //reset offset left and top
          offsetTop += cubeHeight;
          offsetLeft = 0;
        } else if (i) {
          offsetLeft += cubeWidth;
        }

        c[i].parent.style.height = cubeHeight + 'px';
        c[i].parent.style.width = cubeWidth + 'px';
        c[i].parent.style.left = offsetLeft + 'px';
        c[i].parent.style.top = offsetTop + 'px';

        positionElement(c[i].parent, cubeWidth, cubeHeight, offsetTop, offsetLeft);

        positionElement(c[i].front.bg, targetElementWidth, targetElementHeight, -offsetTop, -offsetLeft);

        positionElement(c[i].back.bg, targetElementWidth, targetElementHeight, -offsetTop, -offsetLeft);
                
      }
    }

    self.animate = function() {
      if (supportsTransition) {
            
        self.isAnimating = true;
        // dhtml.external.expand && dhtml.external.expand();

        toggleClass(targetElement, 'flip');
      } else {

        if (nextSlide > targetStructure.backgrounds.length - 1) {
          nextSlide = 0;
        }

        var previousSlide = nextSlide === 0 ? targetStructure.backgrounds.length - 1 : nextSlide - 1; 

        removeClass(targetStructure.slides[previousSlide], 'active');
        addClass(targetStructure.slides[nextSlide], 'active');
            
        nextSlide++;
      }
    };

    function createAnimation() {
        
      targetStructure.slides[lastSideIndex].back.el.addEventListener(transitionEvent, function() {

        var side = (hasClass(targetElement, 'flip') ? 'front' : 'back');

        if (nextSlide > targetStructure.backgrounds.length - 1) {
          nextSlide = 0;
        }

        for (var i = 0; i < targetStructure.slides.length; i++) {
          targetStructure.slides[i][side].bg.style.backgroundImage = 'url(' + targetStructure.backgrounds[nextSlide] + ')';
        }

        nextSlide++;

        self.isAnimating = false;
        
        // dhtml.external.close && dhtml.external.close();

      });
    }

    self.init = function() {

      if (supportsTransition) {
        positionElements(createStructure());

        createAnimation();

        /* 
         * maybe throttle/debounce it 
        */

        window.onresize = function() {
            
          targetElementWidth = getWindowWidth(maxWidth);
          targetElementHeight = Math.floor(targetElementWidth * aspectRatio);

          positionElements(targetStructure.slides);
        };
        
      } 

      return self;
    };
  }

  var wrapper = document.getElementById('wrapper');

  // clickTag
  var clickTAGvalue = dhtml.getVar('clickTAG', 'http://www.example.com');
  var landingpagetarget = dhtml.getVar('landingPageTarget', '_blank');

  wrapper.onclick = function() {
    window.open(clickTAGvalue, landingpagetarget);
  }
  
  /* You shall be responsive orr maybe not */
  if (dhtml.external && dhtml.external.resize) dhtml.external.resize ('100%', '100%');

  
  // var SingleExpanding = Adform.Component.SingleExpanding;
  // var settings = {
  //   x: 10, //collapsed part x
  //   y: 10,  //collapsed part y
  //   collapsedWidth: 970,
  //   collapsedHeight: 250,
  //   expandEasing: SingleExpanding.regularEaseOut,
  //   collapseEasing: SingleExpanding.regularEaseIn,
  //   expandTime: 0,
  //   collapseTime: 0
  // };

  // SingleExpanding.init(settings);

  var banner = new Venetian3d(wrapper, {random: true}).init();

  var animation = setInterval(banner.animate, 4000);
        
  wrapper.addEventListener('touchend', function(event) { 
    if (!banner.isAnimating) {
      clearInterval(animation);
      banner.animate();
    }
  }, false);
 
})(window);

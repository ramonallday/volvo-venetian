var gulp = require('gulp');
var inlinesource = require('gulp-inline-source');
var less         = require('gulp-less');
var jshint       = require('gulp-jshint');
var runSequence  = require('run-sequence');
var gulpif       = require('gulp-if');
var imagemin     = require('gulp-imagemin');
var zip = require('gulp-zip');
var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var autoprefixer = new LessPluginAutoPrefix({ browsers: [
        'last 2 versions',
        'ie 8',
        'ie 9',
        'android 2.3',
        'android 4',
        'opera 12'
      ] });
 

var manifest = require('asset-builder')('./src/assets/manifest.json');
var path = manifest.paths;
var globs = manifest.globs;




gulp.task('less', function() {

    return gulp.src(path.source + 'styles/main.less')
    .pipe(less({
    plugins: [autoprefixer]
  }))
    .pipe(gulp.dest(path.source + 'styles/'))

});


gulp.task('jshint', function() {
  return gulp.src(path.source + 'scripts/main.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

// ### Images
// `gulp images` - Run lossless compression on all the images.
gulp.task('images', function() {
  return gulp.src(globs.images)
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
    }))
    .pipe(gulp.dest(path.dist + 'images'));
    //.pipe(browserSync.stream());
});


gulp.task('manifest', function() {

  return gulp.src(path.manifest)
        .pipe(gulp.dest('./dist'));

});

gulp.task('html', function() {

  return gulp.src(path.index)
        .pipe(inlinesource({ compress: false }))
        .pipe(gulp.dest('./dist'));

});

gulp.task('styles', function() {
  runSequence(
    ['less'],
    ['html']
  );
});

gulp.task('scripts', function() {
  runSequence(
    ['jshint'],
    ['html']
  );
});


gulp.task('watch', function() {

  gulp.watch([path.index], ['html', 'manifest']);
  gulp.watch([path.source + 'styles/main.less'], ['styles', 'manifest']);
  gulp.watch([path.source + 'scripts/**/*'], ['scripts', 'manifest']);
  //gulp.watch([path.source + 'fonts/**/*'], ['fonts']);
  gulp.watch([path.source + 'images/**/*'], ['images', 'manifest']);
});

gulp.task('default', function () {
    return gulp.src('./dist/*/**')
        .pipe(zip('banner.zip'))
        .pipe(gulp.dest('./build'));
});